<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Pricing</title>
</head>
<body>
    <div class="container3">
        <div class="header">
            <h1>Public Sale</h1>
            <h2>16 Dec,2021</h2>
        </div>
    </div>
    <div class ="container">
          <div class="card">
            <div class="numberCircle">0 min</div>
            <div class ="card-title">
                <h2>0.134 ETH</h2>
            </div>
            <div class="card-content">
                <ul>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> NFT</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Physical art print free shipping</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> 12,000 $12 token</li> 
                    <li><i class="fa fa-plus" aria-hidden="true"></i> 1.6 ETH give away draw</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Participate in a draw to win a meebit</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Life time free access for future presale</li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="numberCircle">30 min</div>
            <div class ="card-title">
                <h2>0.134 ETH</h2>
            </div>
            <div class="card-content">
                <ul>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> NFT</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Physical art print free shipping</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> 12,000 $12 token</li> 
                    <li><i class="fa fa-plus" aria-hidden="true"></i> 1.6 ETH give away draw</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Participate in a draw to win a meebit</li>
                    <li><i class="fa fa-plus" aria-hidden="true"></i> Life time free access for future presale</li>
                </ul>
            </div>
        </div>
       <div class="card">
        <div class="numberCircle">60 min</div>
        <div class ="card-title">
            <h2>0.134 ETH</h2>
          </div>
          <div class="card-content">
            <ul>
                <li><i class="fa fa-circle" aria-hidden="true"></i> NFT</li>
                <li><i class="fa fa-plus" aria-hidden="true"></i> Physical art print free shipping</li>
                <li><i class="fa fa-plus" aria-hidden="true"></i> 12,000 $12 token</li> 
                <li><i class="fa fa-plus" aria-hidden="true"></i> 1.6 ETH give away draw</li>
                <li><i class="fa fa-plus" aria-hidden="true"></i> Participate in a draw to win a meebit</li>
                <li><i class="fa fa-plus" aria-hidden="true"></i> Life time free access for future presale</li>
            </ul>
          </div>
        </div>
    </div>
    <div class="container1">
        <div class="center">
            <button type="button" class="button">Mint</button>
        </div>
      </div>
</body>
</html>
************************CSS*****************************
*{
    margin: 0px ;
    padding: 1px;
    box-sizing: border-box;
    font-family: Arial, Helvetica, sans-serif;
}
.container3{
    display: flex;
    color: white;
    margin-top: 30px;
    text-align: center;
    padding: 30px; 
    left: 50%;
}
.container3 .header{
    position:absolute;
    left: 44%;
}
.container3 .header .h2{
    text-align: center;
    padding:0px;
    color: white;
}

body{
    width: 100%;
    height: 100vh;
    background: #0e2644;
}

.container{
    background-color:#0e2644;
    width: 1100px;
    height: 100%;
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: space-around;
    color: white;
}

.container .card{
    width: 300px;
    height: auto;
    padding: 10px 0;
    text-align: left;
    background: linear-gradient(135deg,#000428  0%,#004e92 100%);
    border-radius: 15px;
    box-shadow: 2px 5px 15px black;
    transition: transform 0.3s;
}
.container .card .numberCircle {
    width: 80px;
    height: 80px;
    border-radius: 50%;
    text-align: center;
    font-size: 19px;
    border: 3px solid skyblue;
    color: black;
    background: white;
    margin: -33px auto;
    z-index: 1;
    transform: translateY(-21%);
    line-height: 75px;
  }

.container .card:hover{
    transform: scale(1.050);
}

.container .card .card-title{
    text-align: center;
    padding: 30px 20px;
    border-bottom: 2px solid gray;
    font-size: 25px;
}

.container .card .card-title h2{
    font-size: 35px;
    padding-bottom: 15px;
    color:white;
}

.container .card .card-title p span{
    font-size: 40px;
    padding: 0 5px;
}

.container .card .card-content ul{
    padding: 10px 30px;
    padding-bottom: 20px;
}

.container .card .card-content ul li{
    list-style: none;
    padding: 10px 0;
}

.container .card .card-content ul li i{
    margin-right: 4px;
}
.container1 .center .button{
    width: 130px;
    padding: 10px 20px;
    background: white;
    border-radius: 20px;
    margin-bottom: 10px;
    outline: none;
    border: none;
    cursor: pointer;
    transition: 0.3s;
    text-decoration: none;
    color:black;
    font-size: 25px;
}
.center{ 
    margin: -1px;
    position: absolute;
    top: 0%;
    left: 49%;
    padding: 28px 30px;
    transform: translate(-48%, -56%);
}
.container1 { 
    height: 100px;
    position: relative; 
  }
  .container1 .center .button:hover{
    background: white;
    letter-spacing: 1.2px;
  }